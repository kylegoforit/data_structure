﻿using System;

namespace MyQuickSort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[] { 5, 6, 7, 4, 2, 3, 1 };
            MyQuickSort(arr);
        }

        public static void MyQuickSort(int[] arr)
        {
            MyQuickSort(arr, 0, arr.Length - 1);
        }

        private static void MyQuickSort(int[] arr, int start, int end)
        {
            int middle = Swap(arr, start, end);
            if (start < middle - 1)
            {
                MyQuickSort(arr, start, middle - 1);
            }

            if (middle < end)
            {
                MyQuickSort(arr, middle, end);
            }
        }

        private static int Swap(int[] arr, int start, int end)
        {
            int pivot = arr[(start + end) / 2];
            while (start <= end)
            {
                while (arr[start] < pivot)
                {
                    start++;
                }
                while (arr[end] > pivot)
                {
                    end--;
                }
                if (start <= end)
                {
                    int temp = arr[start];
                    arr[start] = arr[end];
                    arr[end] = temp;
                    start++;
                    end--;
                }
            }

            return start;
        }
    }
}
