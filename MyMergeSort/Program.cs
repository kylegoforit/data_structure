﻿using System;

namespace MyMergeSort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[] { 4,2,6,3,7,8,5,1};
            MyMergeSort(arr);
        }

        static void MyMergeSort(int[] arr)
        {
            int[] temp = new int[arr.Length];
            MyMergeSort(arr, temp, 0, arr.Length - 1);
        }

        static void MyMergeSort(int[] arr, int[] temp, int start, int end)
        {
            if (start < end)
            {
                int middle = (start + end) / 2;
                MyMergeSort(arr, temp, start, middle);
                MyMergeSort(arr, temp, middle + 1, end);
                MyMerge(arr, temp, start, middle, end);
            }
        }

        static void MyMerge(int[] arr, int[] temp, int start, int middle, int end)
        {
            int partition1 = start;
            int partition2 = middle + 1;
            int index = start;

            for (int i = 0; i < arr.Length; i++)
            {
                temp[i] = arr[i];
            }

            while (partition1 <= middle && partition2 <= end)
            {
                if (temp[partition1] <= temp[partition2])
                {
                    arr[index] = temp[partition1++];
                }
                else
                {
                    arr[index] = temp[partition2++];
                }
                index++;
            }

            for (int i = 0; i <= middle - partition1; i++)
            {
                arr[index++] = temp[i+ partition1];
            }
        }
    }
}
