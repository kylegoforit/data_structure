﻿
using System;

namespace MyHeapSort
{
    class Program
    {
        static void Main(string[] args)
        {
            HeapSort heapSort = new HeapSort();

            heapSort.Insert(9);
            heapSort.Insert(7);
            heapSort.Insert(6);
            heapSort.Insert(5);
            heapSort.Insert(4);
            heapSort.Insert(3);
            heapSort.Insert(2);
            heapSort.Insert(2);
            heapSort.Insert(1);
            heapSort.Insert(3);
            //heapSort.Insert(8);
            var h = heapSort.GetHeap();

            var max = heapSort.GetMaxValue();
            h = heapSort.GetHeap();
        }
    }
}
