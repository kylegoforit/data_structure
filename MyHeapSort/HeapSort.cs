﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyHeapSort
{
    public class HeapSort
    {
        private int?[] _heap;
        private int index;
        public HeapSort()
        {
            _heap = new int?[8];
            index = 0; // 힙 정렬은 1부터 시작함.
        }

        public int?[] GetHeap()
        {
            return this._heap;
        }

        public void Insert(int key)
        {
            index++;
            int p = index;
            if (index == 1)
            {
                _heap[index] = key;
                return;
            }

            if (_heap.Length <= index)
            {
                var temp = _heap;
                _heap = new int?[_heap.Length * 2];
                for (int i = 0; i < temp.Length; i++)
                {
                    _heap[i] = temp[i];
                }
            }

            _heap[index] = key;

            while (p > 1)
            {
                if (_heap[p/2] >= _heap[p])
                {
                    break;
                }

                int? temp = _heap[p];
                _heap[p] = _heap[p/2];
                _heap[p/2] = temp;
                p = p / 2;
            }
        }

        public int? GetMaxValue()
        {
            if (index == 0)
            {
                return null;
            }

            int? max = _heap[1];
            _heap[1] = _heap[index];
            _heap[index--] = null;

            int p = 2;
            while (p <= index)
            {
                if (_heap[p + 1] != null)
                {
                    if (_heap[p] < _heap[p + 1]  && _heap[p/2] < _heap[p + 1])
                    {
                        p++;
                    }
                }

                if (_heap[p/2] < _heap[p])
                {
                    int? temp = _heap[p / 2];
                    _heap[p / 2] = _heap[p];
                    _heap[p] = temp;
                }

                p = p * 2;
            }

            return max;
        }
    }
}
